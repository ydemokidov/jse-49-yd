package com.t1.yd.tm;

import com.t1.yd.tm.component.Bootstrap;
import org.jetbrains.annotations.NotNull;

public class TaskManagerLogger {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
