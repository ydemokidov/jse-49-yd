package com.t1.yd.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.t1.yd.tm.api.service.ILoggerService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService implements ILoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        final String table = event.get("table").toString();
        final byte[] bytes = text.getBytes();
        final File file = new File(table);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }


}
