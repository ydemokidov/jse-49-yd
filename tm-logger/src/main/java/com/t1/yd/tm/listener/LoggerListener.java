package com.t1.yd.tm.listener;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.service.LoggerService;
import lombok.SneakyThrows;
import org.eclipse.persistence.oxm.annotations.XmlElementNillable;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        loggerService.log(textMessage.getText());
    }

}
