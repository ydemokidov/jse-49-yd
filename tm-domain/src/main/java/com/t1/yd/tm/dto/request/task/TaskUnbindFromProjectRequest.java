package com.t1.yd.tm.dto.request.task;

import com.t1.yd.tm.dto.request.AbstractUserRequestById;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskUnbindFromProjectRequest extends AbstractUserRequestById {

    private String projectId;

}
