package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class ProjectClearResponse extends AbstractResultResponse {

    public ProjectClearResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public ProjectClearResponse() {
    }

}
