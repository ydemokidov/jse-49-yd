package com.t1.yd.tm.command.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.request.project.ProjectListRequest;
import com.t1.yd.tm.dto.response.project.ProjectListResponse;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project_list";

    @NotNull
    public static final String DESCRIPTION = "Show list of projects";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setSort(sort.toString());
        request.setToken(getToken());

        @NotNull final ProjectListResponse response = getProjectEndpointClient().listProjects(request);

        @NotNull final List<ProjectDTO> projectDTOS = response.getProjectDTOS();

        int index = 1;
        for (@NotNull final ProjectDTO projectDTO : projectDTOS) {
            System.out.println(index + ". " + projectDTO);
            index++;
        }
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
