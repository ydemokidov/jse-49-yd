package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.UserRegistryRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_registry";

    @NotNull
    private final String description = "Create new user";

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE NEW USER]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        @NotNull final String email = TerminalUtil.nextLine();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setEmail(email);
        request.setPassword(password);

        @NotNull final UserDTO userDTO = getUserEndpoint().registry(request).getUserDTO();

        System.out.println("[USER CREATED]");
        showUser(userDTO);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
