package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import com.t1.yd.tm.exception.user.IncorrectLoginOrPasswordException;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class UserDTOEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final String USER_LOGIN = "USER_LOGIN";
    @NotNull
    private final String USER_PASSWORD = "USER_PASSWORD";
    @NotNull
    private final String USER_EMAIL = "USER_EMAIL@mail.ru";
    @NotNull
    private final String USER_FIRST_NAME = "USER_FIRST_NAME";
    @NotNull
    private final String USER_LAST_NAME = "USER_FIRST_NAME";
    @NotNull
    private final String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";
    @NotNull
    private final String ERROR_LOGIN_PASSWORD = new IncorrectLoginOrPasswordException().getMessage();
    @NotNull
    private final String ERROR_USER_NOT_FOUND = new UserNotFoundException().getMessage();
    @NotNull
    private final String ERROR_NOT_LOGIN = new AccessDeniedException().getMessage();
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Nullable
    private String token;
    @NotNull
    private UserDTO testUserDTO;
    @NotNull
    private IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private UserDTO testUserCreate() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest();
        removeRequest.setToken(token);
        removeRequest.setLogin(USER_LOGIN);
        userEndpoint.remove(removeRequest);
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(USER_LOGIN);
        registryRequest.setPassword(USER_PASSWORD);
        registryRequest.setEmail(USER_EMAIL);
        @Nullable final UserDTO userDTOCreated = userEndpoint.registry(registryRequest).getUserDTO();
        Assert.assertNotNull(userDTOCreated);
        return userDTOCreated;
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
        saveBackup(token);
        testUserDTO = testUserCreate();
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void userChangePassword() {
        @NotNull final UserPasswordChangeRequest request = new UserPasswordChangeRequest();
        request.setToken(token);
        request.setNewPassword(USER_PASSWORD + "1");
        userEndpoint.passwordChange(request);
        thrown.expectMessage(ERROR_LOGIN_PASSWORD);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userLock() {
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setToken(token);
        request.setLogin(USER_LOGIN);
        userEndpoint.lock(request);
        thrown.expectMessage(ERROR_LOGIN_PASSWORD);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userUnlock() {
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setToken(token);
        request.setLogin(USER_LOGIN);
        userEndpoint.lock(request);
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest();
        unlockRequest.setToken(token);
        unlockRequest.setLogin(USER_LOGIN);
        userEndpoint.unlock(unlockRequest);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userRemove() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest();
        request.setToken(token);
        request.setLogin(USER_LOGIN);
        userEndpoint.remove(request);
        thrown.expectMessage(ERROR_USER_NOT_FOUND);
        login(USER_LOGIN, USER_PASSWORD);
    }

    @Test
    public void userUpdateProfile() {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setToken(token);
        request.setFirstName(USER_FIRST_NAME);
        request.setLastName(USER_LAST_NAME);
        request.setMidName(USER_MIDDLE_NAME);
        userEndpoint.updateProfile(request);
        @NotNull final UserGetProfileRequest requestProfile = new UserGetProfileRequest();
        requestProfile.setToken(token);
        @Nullable final UserDTO userDTOFound = authEndpoint.getProfile(requestProfile).getUserDTO();
        Assert.assertEquals(USER_FIRST_NAME, userDTOFound.getFirstName());
        Assert.assertEquals(USER_LAST_NAME, userDTOFound.getLastName());
        Assert.assertEquals(USER_MIDDLE_NAME, userDTOFound.getMiddleName());
    }

    @Test
    public void userLogout() {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(token);
        authEndpoint.logout(request);
        thrown.expectMessage(ERROR_NOT_LOGIN);
        @NotNull final UserGetProfileRequest requestProfile = new UserGetProfileRequest();
        authEndpoint.getProfile(requestProfile).getUserDTO();
    }

    @Test
    public void userProfile() {
        @NotNull final String testToken = login(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserGetProfileRequest request = new UserGetProfileRequest();
        request.setToken(testToken);
        @Nullable final UserDTO userDTO = authEndpoint.getProfile(request).getUserDTO();
        Assert.assertEquals(USER_LOGIN, userDTO.getLogin());
        Assert.assertEquals(USER_EMAIL, userDTO.getEmail());
    }

}