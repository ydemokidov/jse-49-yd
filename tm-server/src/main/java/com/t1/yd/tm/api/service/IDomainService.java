package com.t1.yd.tm.api.service;

public interface IDomainService {

    void dataFasterXmlXmlLoad();

    void dataFasterXmlXmlSave();

    void dataBackupLoad();

    void dataBackupSave();

    void dataBase64Load();

    void dataBase64Save();

    void dataBinarySave();

    void dataBinaryLoad();

    void dataFasterXmlJsonLoad();

    void dataFasterXmlJsonSave();

    void dataFasterXmlYmlLoad();

    void dataFasterXmlYmlSave();

    void dataJaxbJsonLoad();

    void dataJaxbJsonSave();

    void dataJaxbXmlLoad();

    void dataJaxbXmlSave();

}
