package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoSessionRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.ISessionDtoService;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.repository.dto.SessionDtoRepository;
import org.jetbrains.annotations.NotNull;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityManager;

public class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, IDtoSessionRepository> implements ISessionDtoService {

    public SessionDtoService(@NotNull final IConnectionService connectionService, @NotNull final ILoggerService loggerService) {
        super(connectionService,loggerService);
    }

    @NotNull
    @Override
    protected IDtoSessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }


    @Override
    public @NotNull SessionDTO update(@NotNull SessionDTO entity) {
        throw new NotImplementedException();
    }

}
