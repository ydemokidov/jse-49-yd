package com.t1.yd.tm.api.repository.model;

import com.t1.yd.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}