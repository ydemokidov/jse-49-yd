package com.t1.yd.tm;

import com.t1.yd.tm.component.Bootstrap;
import org.jetbrains.annotations.NotNull;

public final class TaskManagerServer {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
