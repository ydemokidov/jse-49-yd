package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.dto.ISessionDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.service.dto.SessionDtoService;
import com.t1.yd.tm.service.dto.UserDtoService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.SessionTestData.*;
import static com.t1.yd.tm.constant.UserTestData.*;

public class SessionDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ILoggerService loggerService = new LoggerService(connectionService);

    private ISessionDtoService service = new SessionDtoService(connectionService,loggerService);

    private IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService,loggerService);

    @Before
    public void initRepository() {
        service.clear();
        userDtoService.clear();

        userDtoService.add(ALL_USER_DTOS);
    }

    @Test
    public void add() {
        service.add(SESSION_DTO_1);
        Assert.assertEquals(SESSION_DTO_1.getId(), service.findOneById(SESSION_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        Assert.assertEquals(SESSION_DTO_1.getId(), service.findAll(USER_DTO_1.getId()).get(0).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_SESSION_DTOS);
        Assert.assertEquals(ALL_SESSION_DTOS.size(), service.findAll().size());
    }

    @Test
    public void clear() {
        service.add(ALL_SESSION_DTOS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);
        Assert.assertEquals(1, service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void existsById() {
        service.add(SESSION_DTO_1);
        Assert.assertTrue(service.existsById(SESSION_DTO_1.getId()));
        Assert.assertFalse(service.existsById(SESSION_DTO_2.getId()));
    }

    @Test
    public void removeById() {
        service.add(ALL_SESSION_DTOS);
        service.removeById(SESSION_DTO_1.getId());
        Assert.assertEquals(ALL_SESSION_DTOS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(SESSION_DTO_1.getId()));
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);

        Assert.assertEquals(SESSION_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), SESSION_DTO_1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), SESSION_DTO_1.getId()));
    }

    @After
    public void clearData() {
        service.clear();
        userDtoService.clear();
    }

}
