package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.service.dto.UserDtoService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private ISaltProvider saltProvider = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ILoggerService loggerService = new LoggerService(connectionService);

    private IUserDtoService service;

    @Before
    public void initRepository() {
        service = new UserDtoService(connectionService, saltProvider,loggerService);
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER_DTO_1);
        Assert.assertEquals(USER_DTO_1.getId(), service.findOneById(USER_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_USER_DTOS);
        Assert.assertEquals(ALL_USER_DTOS.size(), service.findAll().size());
        Assert.assertEquals(USER_DTO_1.getId(), service.findOneById(USER_DTO_1.getId()).getId());
    }


    @Test
    public void clear() {
        service.add(ALL_USER_DTOS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findByLogin() {
        service.add(USER_DTO_1);
        service.add(ADMIN);
        Assert.assertEquals(USER_DTO_1.getId(), service.findByLogin(USER1_LOGIN).getId());
    }

    @Test
    public void findByEmail() {
        service.add(USER_DTO_1);
        Assert.assertEquals(USER_DTO_1.getId(), service.findByEmail(USER1_EMAIL).getId());
    }

    @Test
    public void existsByLogin() {
        service.add(USER_DTO_1);
        Assert.assertTrue(service.isLoginExist(USER1_LOGIN));
        Assert.assertFalse(service.isLoginExist(ADMIN_LOGIN));
    }

    @Test
    public void existsByEmail() {
        service.add(USER_DTO_1);
        Assert.assertTrue(service.isEmailExist(USER1_EMAIL));
        Assert.assertFalse(service.isEmailExist(ADMIN_EMAIL));
    }

    @Test
    public void lockByLogin() {
        service.add(USER_DTO_1);
        service.lockByLogin(USER1_LOGIN);
        @NotNull final UserDTO tmpUserDTO = service.findByLogin(USER1_LOGIN);
        Assert.assertTrue(tmpUserDTO.getLocked());
    }

    @Test
    public void unlockByLogin() {
        service.add(USER_DTO_1);
        service.lockByLogin(USER1_LOGIN);
        service.unlockByLogin(USER1_LOGIN);
        @NotNull final UserDTO tmpUserDTO = service.findByLogin(USER1_LOGIN);
        Assert.assertFalse(tmpUserDTO.getLocked());
    }

    @Test
    public void removeByLogin() {
        service.add(USER_DTO_1);
        service.removeByLogin(USER1_LOGIN);
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void removeByEmail() {
        service.add(USER_DTO_1);
        service.removeByEmail(USER1_EMAIL);
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void updateUser() {
        @NotNull final String lastName = "lastname";
        @NotNull final String fstName = "fstname";
        @NotNull final String midName = "midname";
        service.add(USER_DTO_1);
        service.updateUser(USER_DTO_1.getId(), fstName, lastName, midName);
        Assert.assertEquals(lastName, service.findOneById(USER_DTO_1.getId()).getLastName());
        Assert.assertEquals(fstName, service.findOneById(USER_DTO_1.getId()).getFirstName());
        Assert.assertEquals(midName, service.findOneById(USER_DTO_1.getId()).getMiddleName());
    }

    @After
    public void clearData() {
        service.clear();
    }

}
